## What is a Dockerfile?
- A Dockerfile is text document that contains all the commands a user could call on the command line to assemble an image.
- Using docker build users can create an automated build that executes several command-line instructions in succession.

## Demo Project
- Dockerize Nodejs application and push to private Docker registry

### Technologies used
Docker, Node.js, Amazon ECR

### Project Description
- Write Dockerfile to build a Docker image for a Nodejs application
- Create private Docker registry on AWS (Amazon ECR)
- Push Docker image to this private repository

#### ECR Push
Build the image
`docker build -t my-app:1.0 .`

TAG the image
`docker tag my-app:1.0 345008190010.dkr.ecr.us-east-1.amazonaws.com/my-app:1.0`

Push the Image to ECR
`docker push 345008190010.dkr.ecr.us-east-1.amazonaws.com/my-app:1.0`
