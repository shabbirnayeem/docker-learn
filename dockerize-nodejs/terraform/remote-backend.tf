terraform {
  backend "s3" {
    bucket = "sn-labs-devops"
    key    = "docker/ecr.tfstate"
    region = "us-east-1"
  }
}
