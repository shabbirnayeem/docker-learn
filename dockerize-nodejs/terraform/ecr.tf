resource "aws_ecr_repository" "foo" {
  name                 = "my-app"

  image_scanning_configuration {
    scan_on_push = true
  }
}