## Demo Project
Use Docker for local development

## Technologiesused:
Docker, Node.js, MongoDB, MongoExpress

## Project Description:
- Create Dockerfile for Nodejs application and build Docker image
- Run Nodejs application in Docker container and connect to
- MongoDB database container locally. Also run MongoExpress container as a UI of the MongoDB
  database.

## Steps to complete the Project

### Pull mongodb image 
`docker pull mongo`

### Pull mongo-express image
`docker pull mongo-express`

### Create mongo-network
`docker network create mongo-network`

### Start mongodb container
```	
docker run -d \
-p 27017:27017 \
-e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
-e MONGO_INITDB_ROOT_PASSWORD=[PASSWORD] \
--name mongodb \
--net mongo-network \
mongo
```

### Start mongo-express container
```
docker run -d \
-p 8081:8081 \
-e ME_CONFIG_MONGODB_ADMINUSERNAME=mongoadmin \
-e ME_CONFIG_MONGODB_ADMINPASSWORD=[PASSWORD] \
-e ME_CONFIG_MONGODB_SERVER=mongodb \
--net mongo-network \
--name mongo-express \
mongo-express
```


