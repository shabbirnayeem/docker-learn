## What is Docker Volumes?
- Docker volumes are files systems mounted on Docker containers to preserve data generated by the running container.
- The volumes are stored on the host independent of the container life cycle.
- This allows users to back up data and share file systems between containers easily.

### Where Docker Volume located on the host machine?
- Windows: `C:\ProgramData\docker\volumes`
- Linux: `/var/lib/docker/volumes`
- MAC: `/var/lib/docker/volumes`

## Demo Project
Persist data with Docker Volumes

### Technologiesused
- Docker, Node.js, MongoDB

### Project Description
- Persist data of a MongoDB container by attaching a Docker volume to it