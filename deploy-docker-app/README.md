## Demo Project
Deploy Docker application on a server with Docker Compose

### Technologiesused
- Docker, Amazon ECR, Node.js, MongoDB, MongoExpress

### Project Description
- Copy Docker-compose file to remote server
- Login to private Docker registry on remote server to fetch our app image
- Start our application container with MongoDB and MongoExpress services using docker compose