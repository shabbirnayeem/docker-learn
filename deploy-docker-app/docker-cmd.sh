#!/bin/bash

app_name="my-app"
app_tag="1.8"

# build the image
docker build -t $app_name:$app_tag ../Dockerfile

# tag the image
docker tag $app_name:$app_tag 345538798040.dkr.ecr.us-east-1.amazonaws.com/$app_name:$app_tag

# push the image
docker push 345538798040.dkr.ecr.us-east-1.amazonaws.com/$app_name:$app_tag

# run docker-compose
docker-compose -f deploy-docker-app/mongo.yaml up