terraform {
  backend "s3" {
    bucket = "sn-labs-devops"
    key    = "docker/deploy-docker-app/ecr.tfstate"
    region = "us-east-1"
  }
}
