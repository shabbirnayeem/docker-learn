## Demo Project
Create Docker repository on Nexus and push to it

### Technologiesused
- Docker, Nexus, DigitalOcean, Linux

### Project Description
- Create Docker hosted repository on Nexus
- Create Docker repository role on Nexus
- Configure Nexus, DigitalOcean Droplet and Docker to be able to push to Docker repository
- Build and Push Docker image to Docker repository on Nexus


## Configure Nexus
- Create docker repository on Nexus
    - Log into Nexus --> Create repository --> find "docker hosted" repository
        image.png
- Create a new role, that has docker privileges to access docker hosted repository
    image.png
- Assign the role to the user
- Docker Login to Nexus Docker repo
    - Before docker into nexus docker repo we need to configure few things on the Nexus side
        - Create an HTTP connector at specified port
            - Depending on the connection type
                - HTTP: Normally used if the server is behind a secure proxy.
                - HTTPS: Create an HTTPS connector at specified port. Normally used if the server is configured for https.
        - Docker repo cannot use the same post as the server nexus hosted on
            - In this case, the endpoint for docker login will be 134.192.19.17:8083
            - Make sure ports are open on the nexus server Firewall
    - Configure Realms on Nexus:
        - This realm permits users with previously generated bearer tokens to publish npm packages.
            - For Docker Add the `Docker Bearer Token to Realm`
- Configure Docker to allow push and pull from HTTP unsecure repo
    - By default docker only allows HTTPS connection
    - Configure Docker client to allow our docker-hosted from Nexus
    - How configure docker client to allow unsecure repo?
        https://docs.docker.com/registry/insecure/
    - For Edit the daemon.json file
        - Linux:
            - Edit the daemon.json file
            - Default location `/etc/docker/daemon.json`
        - Windows:
            - Default location `C:\ProgramData\docker\config\daemon.json`
        - If the daemon.json file does not exist, create it. Assuming there are no other settings in the file, it should have the following contents:
            `"insecure-registries" : ["myregistrydomain.com:5000"]`
- Logged in to Nexus Docker Repo
    - `docker login 134.192.19.17:8083`
- Pushed Docker Image to Nexus Repo
    - Build the image
        `docker build -t my-image:1.0 .`
    - Tag the image
        `docker tag my-image:1.0 134.192.19.17:8083/my-image:1.0`
    - Push the imahe
        `docker push 134.192.19.17:8083/my-image:1.0`
    - Pull the image from nexus repo
        `curl -u nayeem:admin123 -X GET '134.192.19.17:8081/service/rest/v1/components?repository=docker-hosted'`