## Demo Project
Deploy Nexus as Docker container

### Technologiesused
- Docker, Nexus, DigitalOcean, Linux, Terraform

### Project Description
- Create and Configure Droplet
- Set up and run Nexus as a Docker container