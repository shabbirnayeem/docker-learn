terraform {
  backend "s3" {
    bucket = "sn-labs-devops"
    key    = "docker/demo-docker-nexus-DO/nexus-server.tfstate"
    region = "us-east-1"
  }
}
