data "digitalocean_ssh_key" "lab_key" {
  name = "lab_key"
}

resource "digitalocean_droplet" "nexus-server" {
  image  = "ubuntu-22-04-x64"
  name   = "docker-nexus"
  region = "nyc1"
  size   = "s-2vcpu-4gb"
  ssh_keys = [
    data.digitalocean_ssh_key.lab_key.id,
  ]

  connection {
    host        = self.ipv4_address
    user        = "root"
    type        = "ssh"
    private_key = file(var.pvt_key)
    timeout     = "2m"
  }

  provisioner "file" {
    source      = "../docker-nexus.yaml"
    destination = "/root/docker-nexus.yaml"
  }

  provisioner "file" {
    source      = "../server-setup.sh"
    destination = "/root/server-setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod u+x server-setup.sh",
      "./server-setup.sh"
    ]
  }

}

resource "digitalocean_firewall" "nexus-server-fw" {
  name        = "nexus-server-fw"
  droplet_ids = [digitalocean_droplet.nexus-server.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = [var.my_ip]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "8081"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

}

output "nexus-server-ip" {
  value = digitalocean_droplet.nexus-server.ipv4_address
}
