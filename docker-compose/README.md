## What is docker compose?
- Compose is a tool for defining and running multi-container Docker applications. 
- With Compose, you use a YAML file to configure your application's services.

## Demo Project
Docker Compose - Run multiple Docker containers

### Technologiesused
- Docker, MongoDB, MongoExpress

### Project Description
- Write Docker Compose file to run MongoDB and MongoExpress containers